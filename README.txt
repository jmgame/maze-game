Authors: Deevy Bhimani and Hunter Borlik
Date: 5/23/14

This game is a computer version of real life mazes often made uses bushes. It is a 3D first person maze. This program can be used by anyone who wants to play this game. The primary features of this game are the different colored cubes which challenge players, and the inability to see a map, which pushes players to do their best in order to win.

Controls:
W - Move Forward
A - Move left
S - Move Back
D - Move esc - Quit
LCTRL + R - Restart at Beginning

Class List:
_Main: Controls window,puts the whole program together, allows program to run
CameraControl: First person camera
DisplayUnit - Creates the display for the game
GameObject - Provides a base for an object in the game
Menu - Creates the main menu
Tile - Holds the values for drawing one tile and allows the tile to be drawn
WorldLoader - Reads a text file to build the maze

Responsibilities:
_Main - Deevy and Hunter
CameraControl - Hunter
DisplayUnit - Hunter
GameObject - Hunter
Menu - Deevy
Tile - Hunter
WorldLoader - Deevy and Hunter
MazeDesign- Deevy
README - Deevy
UML - Deevy