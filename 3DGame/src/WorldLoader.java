/**
 * @author Hunter Borlik
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.lwjgl.Sys;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

public class WorldLoader
{
    private long lastFrameTime;
    private Scanner scanner;
    private PrintWriter writer;
    private Tile tile = new Tile(1f, 45f, 60f);
    private ArrayList<Float> posList;
    private float[] pos;
    
    public WorldLoader(final String worldFileName)
    {
	posList = new ArrayList<Float>();
	boolean nextLine = true, fileFound = true;
	String line = "";
	try
	{
	    System.out.println("WorldLoader: Finding world file " + worldFileName + ".txt");
	    File file = new File(worldFileName + ".txt");
	    scanner = new Scanner(file);
	}catch(FileNotFoundException source)
	{
	    System.out.println("WorldLoader:No World file found (creating new)");
	    try
	    {
		writer = new PrintWriter(worldFileName + ".txt");
		writer.append("//Tiles format: (x/y/z)\n");
		writer.close();
		fileFound = false;
		System.out.println("WorldLoader: done creating " + worldFileName + ".txt");
	    }catch(FileNotFoundException source2)
	    {
		System.out.println("WorldLoader: Error creating file");
	    }
	}
	
	System.out.println("WorldLoader: Reading world file");
	if(fileFound == true)
	{
	    while(nextLine == true)
	    {
		try
		{
		    line = scanner.nextLine();
		    
		    if(line.startsWith("//") == false)
		    {
			while(line.isEmpty() == false)
			{
			    int secondP = line.indexOf('/');
			    posList.add(Float.parseFloat(line.substring(0, secondP)));
			    line = line.substring(secondP + 1);
			}
		    }
		}catch(NoSuchElementException e)
		{
		    nextLine = false;
		}catch(NullPointerException e)
		{
		    System.out.println("WorldLoader: Problem Reading world file(Scanner is now closed) ");
		}catch(NumberFormatException e)
		{
		    System.out.println("WorldLoader: Format exception " + e.getMessage());
		}
	    }
	}
	
	pos = new float[posList.size()];
	int hldp = 0;//hold point for float array
	for(float a : posList)
	{
	    pos[hldp] = a;
	    hldp++;
	}
	posList = null;
	System.out.println("WorldLoader: Done loading file");
    }
    
    public void render()
    {
	GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // Clear the screen and
									   // depth buffer Buffer
	// GL11.glLoadIdentity(); // Reset The View//only reset view when
	// drawing objects that are part of hud for camera
	
	
	for(int i = 0; i < pos.length; i += 3)
	{
	    GL11.glPushMatrix();
	    tile.draw(pos[i], pos[i + 1], pos[i + 2]);
	    GL11.glPopMatrix();
	}
	
	
    }
    
    public float getDeltaTime()
    {
	long time = (Sys.getTime() * 1000) / Sys.getTimerResolution();
	float delta = (float) (time - lastFrameTime);
	lastFrameTime = time;
	return delta / 1000;
    }
    
    public float[] getPoints()
    {
	return pos;
    }
}
