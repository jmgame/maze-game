/**
 * @author Deevy Bhimani
 */

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class Menu extends JPanel implements ActionListener, MouseListener {
    
    private Image menu;
    private boolean started = false;
    
    private JButton play, exit;
    
    public Menu()
    {
	
	super();
	menu = (new ImageIcon("MazeMenu.png")).getImage();
	
	repaint();
    }
    
    public void paintComponent(Graphics g) {
	
	super.paintComponents(g);
	g.drawImage(menu, 0, 0, 600, 600, this);
	
	/*
	    	play = new JButton("PLAY");
		play.addActionListener(this);
		
		JPanel nothing = new JPanel();

		JPanel buttons = new JPanel(new GridLayout(10, 10, 1, 0));
		buttons.add(nothing);
		buttons.add(play);

		Container c = getContentPane();
		c.setBackground(Color.WHITE);
		c.add(buttons, BorderLayout.CENTER);
	 */
	
    }
    
    public boolean gameStarted() {
	System.out.println("B " + started);
	return started;
    }
    
    public void actionPerformed(ActionEvent e) {
	Object source = e.getSource();
	if (source == play) {
	    started = true;
	} else if (source == exit) {
	    System.exit(0);
	}
    }
    
    public void mouseClicked(MouseEvent arg0)
    {
	started = true;
	System.out.println("A " + started);
    }
    
    public void mouseEntered(MouseEvent arg0) { }
    public void mouseExited(MouseEvent arg0) { }
    public void mousePressed(MouseEvent arg0) { }
    public void mouseReleased(MouseEvent arg0) { }
}
