/**
 * DisplayUnit class:
 * creates a display window to be used with 3d rendering
 * 
 * @author Hunter Borlik
 * Date: 5/12/2014
 */
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

public class DisplayUnit
{
    String windowTitle;
    private int width, height;
    private float fieldOfView;
    
    public DisplayUnit(int w, int h, String title, float fov)
    {
	windowTitle = title;
	width = w;
	height = h;
	fieldOfView = fov;
    }
    
    //creates a display window
    public void createDisplay()
    {
	try
	{
	    Display.setDisplayMode(new DisplayMode(680, 700));
	    //Display.setVSyncEnabled(true);						//Sync the refresh rate of the display with the monitor
	    Display.setTitle(windowTitle);						//title of display window
	    Display.create();								//creates Display
	} catch (LWJGLException e)
	{
	    Sys.alert("Error", "Initialization failed!\n\n" + e.getMessage());
	    System.exit(0);
	}
    }
    
    //initiates the GL11 graphics
    public void initGL11()
    {
	//int width = Display.getDisplayMode().getWidth();
	//int height = Display.getDisplayMode().getHeight();
	
	GL11.glViewport(0, 0, width, height); 						 // Reset The Current Viewport
	GL11.glMatrixMode(GL11.GL_PROJECTION); 						 // Select The Projection Matrix
	GL11.glLoadIdentity(); 								 // Reset The Projection Matrix
	
	GLU.gluPerspective(fieldOfView, ((float) width / (float) height), 0.1f, 100.0f);	 // Calculate The Aspect Ratio Of The Window
	GL11.glMatrixMode(GL11.GL_MODELVIEW);						 // Select The Modelview Matrix
	GL11.glLoadIdentity();								 // Reset The Modelview Matrix
	
    	GL11.glShadeModel(GL11.GL_SMOOTH); 						 // Enables Smooth Shading
    	GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);					 // Black Background
    	GL11.glClearDepth(1.0f); 							 // Depth Buffer Setup
    	GL11.glEnable(GL11.GL_DEPTH_TEST);						 // Enables Depth Testing
    	GL11.glDepthFunc(GL11.GL_LEQUAL);						 // The Type Of Depth Test To Do
    	GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);		 // Really Nice Perspective Calculations
    }
    
    public void cleanUp()
    {
	Display.destroy();
    }
}
