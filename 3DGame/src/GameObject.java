/**
 * 
 * @author Hunter Borlik
 *
 */
public interface GameObject
{   
    public void draw(float x, float y, float z);
}
