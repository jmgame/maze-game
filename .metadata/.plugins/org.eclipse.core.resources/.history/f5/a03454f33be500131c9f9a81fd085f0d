/**
 * @author Hunter Borlik
 * Date: 5/13/2014
 */
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

public class CameraControl
{
    private float yaw = 0.0f, pitch = 0.0f;
    private int upperLowerLookLimit = 60;
    
    private Vector3f vect;
    private Vector3f velocity;
    private float gravityForce;
    
    private float[] world;
    private final float detectionCollisionOffsetXZYDn = 1.0f;//distance for collision detection on every side but the top
    private final float detectionCollisionOffsetYUp = 1.0f;//distance for collision detection on the top
    
    private Vector3f currentCollide;
    private double distanceToCube = 0;
    
    private enum Side
    {
	X,
	Y,
	Z
    }
    private Side[] stack = {null, null};
    private int stackPoint = 0;
    
    
    boolean onspawn = false;
    
    public CameraControl(float x, float y, float z, float gravity, float[] worldArray)
    {
	vect = new Vector3f();
	vect.x = x;
	vect.y = y;
	vect.z = z;
	
	gravityForce = gravity;
	
	world = worldArray;
	
	velocity = new Vector3f();
	velocity.setX(0.0f);
	velocity.setY(0.0f);
	velocity.setZ(0.0f);
	
	currentCollide = new Vector3f();
    }
    
    public void yaw(float yaw)
    {
	this.yaw += yaw;
	if (this.yaw >= 360)this.yaw = 0;//prevents overflow
	if (this.yaw < 0)this.yaw = 359;
    }
    
    public void pitch(float pitch)
    {
	this.pitch += pitch;
	if (this.pitch >= 60)this.pitch = upperLowerLookLimit;
	if (this.pitch <= -60)this.pitch = -upperLowerLookLimit;
    }
    
    public void refresh(float dt)
    {
	//roatate the pitch around the X axis
        GL11.glRotatef(pitch, 1.0f, 0.0f, 0.0f);
        //roatate the yaw around the Y axis
        GL11.glRotatef(yaw, 0.0f, 1.0f, 0.0f);
        
        if(isColliding(vect))
        {
	    //collisionCalc();
        }else
        {
            
            if(velocity.x > 0)
        	velocity.x -= 2.0f * dt * Math.abs(velocity.x / 2);
            if(velocity.x < 0)
        	velocity.x += 2.0f * dt * Math.abs(velocity.x / 2);
            
            if(velocity.z > 0)
        	velocity.z -= 2.0f * dt * Math.abs(velocity.z / 2);
            if(velocity.z < 0)
        	velocity.z += 2.0f * dt * Math.abs(velocity.z / 2);
            
            if(velocity.y > 0)
        	velocity.y -= 2.0f * dt * Math.abs(velocity.y / 2);
            if(velocity.y < 0)
        	velocity.y += 2.0f * dt * Math.abs(velocity.y / 2);
            
            if(velocity.y > 2.0f)
            {
        	velocity.y = 1.9f;
            }else if(velocity.y < -2.0)
            {
        	velocity.y = -1.9f;
            }
        }
        
        
        
        //velocity.y += gravityForce * dt;
        vect.y += velocity.y * dt;
        vect.z += velocity.z * dt;
        vect.x += velocity.x * dt;
        GL11.glTranslatef(vect.x, vect.y, vect.z);
        //System.out.println(vect + " " + velocity);
        
    }
    
    public void setPitchLimit(int degrees)
    {
	upperLowerLookLimit = degrees;
    }
    
    public void moveForward(float distance)
    {
	velocity.x -= distance * (float) Math.sin(Math.toRadians(yaw));//sin function needs the angle in radians
	velocity.z += distance * (float) Math.cos(Math.toRadians(yaw));//cos function needs the angle in radians
    }
    
    public void moveBackward(float distance)
    {
	velocity.x -= distance * (float) Math.sin(Math.toRadians(yaw - 180));//sin function needs the angle in radians
	velocity.z += distance * (float) Math.cos(Math.toRadians(yaw - 180));//cos function needs the angle in radians
    }
    
    public void moveRight(float distance)
    {
	velocity.x -= distance * (float) Math.sin(Math.toRadians(yaw - 90));//sin function needs the angle in radians
	velocity.z += distance * (float) Math.cos(Math.toRadians(yaw - 90));//cos function needs the angle in radians
    }
    
    public void moveLeft(float distance)
    {
	velocity.x -= distance * (float) Math.sin(Math.toRadians(yaw + 90));//sin function needs the angle in radians
	velocity.z += distance * (float) Math.cos(Math.toRadians(yaw + 90));//cos function needs the angle in radians
    }
    
    public void up(float force)
    {
	velocity.y -= force;
    }
    
    public void down(float force)
    {
	velocity.y += force;
    }
    
    public void setGravity(float force)
    {
	gravityForce = force;
    }
    
    public void updateWorldArray(float[] worldArray)
    {
	world = worldArray;
    }
    
    public void setPos(Vector3f newPos)
    {
	vect.x = newPos.x;
	vect.y = newPos.y;
	vect.z = newPos.z;
    }
    
    public boolean isColliding(Vector3f pointPos)
    {
	boolean withinArea = false;
	
	distanceToCube = (long) 0.0;
	for(int i = 0; i < world.length; i += 3)//finds the cube that is the closest to the player
	{
	    double distance = Math.sqrt(Math.pow(vect.x - world[i],2) + Math.pow(vect.y - world[i + 1],2) + Math.pow(vect.z - world[i + 2],2));
	    if(distance < distanceToCube || distanceToCube == 0.0)
	    {
		if(currentCollide.x != world[i] || currentCollide.y != world[i + 1] || currentCollide.z != world[i + 2])
		{
		    stack[0] = null;
		    stack[1] = null;
		    stackPoint = 0;
		}
		
		distanceToCube = distance;
		currentCollide.x = world[i];
		//currentCollide.y = world[i + 1];
		currentCollide.z = world[i + 2];
	    }
	}
	
	if(pointPos.x >= currentCollide.x - detectionCollisionOffsetXZYDn && pointPos.x <= currentCollide.x + detectionCollisionOffsetXZYDn && stack[0] != Side.Y)
	{
	    stack[0] = Side.X;
	    System.out.println("within x");
	}else if(pointPos.z >= currentCollide.z - detectionCollisionOffsetXZYDn && pointPos.z <= currentCollide.z + detectionCollisionOffsetXZYDn && stack[0] != Side.X)
	{
	    stack[0] = Side.Z;
	    System.out.println("within z");
	}
	
	
	if(pointPos.x >= currentCollide.x - detectionCollisionOffsetXZYDn && pointPos.x <= currentCollide.x + detectionCollisionOffsetXZYDn && stack[0] == Side.Y)
	{
	    stack[1] = Side.X;
	    System.out.println("within x2");
	}else if(pointPos.z >= currentCollide.z - detectionCollisionOffsetXZYDn && pointPos.z <= currentCollide.z + detectionCollisionOffsetXZYDn && stack[0] == Side.X)
	{
	    stack[1] = Side.Z;
	    System.out.println("within z2");
	}
	
	
	if(stack[0] == Side.Z && stack[1] == Side.X)
	{
	    velocity.x = -velocity.x;
	    //System.out.println("bounce x1");
	}
	
	if(stack[0] == Side.X && stack[1] == Side.Z)
	{
	    velocity.z = -velocity.z;
	    //System.out.println("bounce y1");
	}
	
	if(stack[0] != null && stack[1] != null)
	{
	    stack[0] = null;
	    stack[1] = null;
	}
	
	if(distanceToCube < 1)
	    withinArea = true;
	
	return withinArea;
    }
    
}
